import { StudentsService } from './../../services/students.service';
import { DataSource } from '@angular/cdk/collections';
import { MatPaginator, MatSort } from '@angular/material';
import { map } from 'rxjs/operators';
import { Observable, of as observableOf, merge } from 'rxjs';
import { Student } from 'src/app/models/student.model';

// TODO: Replace this with your own data model type
// export interface StudentsTableItem {
//   id: number;
//   firstname: string;
//   lastname: string;
//   studentIndex: string;

// }

// TODO: replace this with real data from your application
// const EXAMPLE_DATA: StudentsTableItem[] = [
//   {id: 1, firstname: 'Hydrogen', lastname: 'HAHAH', studentIndex: '3orkf'},
//   {id: 2, firstname: 'Helium', lastname: 'EMMEE', studentIndex: 'weopjt'},
//   {id: 3, firstname: 'Lithium', lastname: 'ELLE', studentIndex: 'cioerjf'},
//   {id: 4, firstname: 'Beryllium', lastname: 'EORROGR', studentIndex: 'weior'},
//   {id: 5, firstname: 'Boron', lastname: 'ELRPG', studentIndex: 'qwoier'},
//   {id: 6, firstname: 'Carbon', lastname: 'FJEOF', studentIndex: 'qweji'},
//   {id: 7, firstname: 'Nitrogen', lastname: 'WJOFO', studentIndex: 'eiqow'},
//   {id: 8, firstname: 'Oxygen', lastname: 'CROJOOGJ', studentIndex: 'croooj'},
//   {id: 9, firstname: 'Fluorine', lastname: 'CJER', studentIndex: 'cjer'},
//   {id: 10, firstname: 'Neon', lastname: 'DWEFO', studentIndex: 'dwefo'},
//   {id: 11, firstname: 'Sodium', lastname: 'EROG', studentIndex: 'erog'},
//   {id: 12, firstname: 'Magnesium', lastname: 'Djurica', studentIndex: 'djurica'}
// ];

/**
 * Data source for the StudentsTable view. This class should
 * encapsulate all logic for fetching and manipulating the displayed data
 * (including sorting, pagination, and filtering).
 */
export class StudentsTableDataSource extends DataSource<Student> {
  data: Student[];

  constructor(private paginator: MatPaginator, private sort: MatSort) {
    super();
  }

  /**
   * Connect this data source to the table. The table will only update when
   * the returned stream emits new items.
   * @returns A stream of the items to be rendered.
   */
  connect(): Observable<Student[]> {
    // Combine everything that affects the rendered data into one update
    // stream for the data-table to consume.
    const dataMutations = [
      observableOf(this.data),
      this.paginator.page,
      this.sort.sortChange
    ];

    // Set the paginator's length
    this.paginator.length = this.data.length;

    return merge(...dataMutations).pipe(map(() => {
      return this.getPagedData(this.getSortedData([...this.data]));
    }));
  }

  /**
   *  Called when the table is being destroyed. Use this function, to clean up
   * any open connections or free any held resources that were set up during connect.
   */
  disconnect() {}

  /**
   * Paginate the data (client-side). If you're using server-side pagination,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getPagedData(data: Student[]) {
    const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
    return data.splice(startIndex, this.paginator.pageSize);
  }

  /**
   * Sort the data (client-side). If you're using server-side sorting,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getSortedData(data: Student[]) {
    if (!this.sort.active || this.sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      const isAsc = this.sort.direction === 'asc';
      switch (this.sort.active) {
        case 'firstname': return compare(a.firstName, b.firstName, isAsc);
        case 'lastname': return compare(a.lastName, b.lastName, isAsc);
        case 'studentIndex': return compare(a.studentIndex, b.studentIndex, isAsc);
        case 'id': return compare(+a.id, +b.id, isAsc);
        default: return 0;
      }
    });
  }
}

/** Simple sort comparator for example ID/Name columns (for client-side sorting). */
function compare(a, b, isAsc) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}


