import { Student } from 'src/app/models/student.model';
import { StudentsService } from './../../services/students.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { StudentsTableDataSource } from './students-table-datasource';

@Component({
  selector: 'app-students-table',
  templateUrl: './students-table.component.html',
  styleUrls: ['./students-table.component.css'],
})
export class StudentsTableComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: StudentsTableDataSource;
  students: Student[];


  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['id', 'firstname', 'lastname', 'studentIndex'];
  constructor(private studentService: StudentsService) { }

  ngOnInit() {
    // this.studentService.getStudents().subscribe(
    //   students => {
    //     this.students = students;
    //   });
    this.students = [
      { 'id': 1, 'firstName': 'Marko', 'lastName': 'Maric', 'studentIndex': 'MM/15', 'exams': null },
      { 'id': 2, 'firstName': 'Pera', 'lastName': 'Peric', 'studentIndex': 'PP/15', 'exams': null },
      { 'id': 3, 'firstName': 'Ana', 'lastName': 'Anic', 'studentIndex': 'AA/15', 'exams': null },
      { 'id': 1, 'firstName': 'Marko', 'lastName': 'Maric', 'studentIndex': 'MM/15', 'exams': null },
      { 'id': 2, 'firstName': 'Pera', 'lastName': 'Peric', 'studentIndex': 'PP/15', 'exams': null },
      { 'id': 3, 'firstName': 'Ana', 'lastName': 'Anic', 'studentIndex': 'AA/15', 'exams': null }
    ];
    this.dataSource = new StudentsTableDataSource(this.paginator, this.sort);
    this.dataSource.data = this.students;
  }
}
