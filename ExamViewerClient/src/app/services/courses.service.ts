import { Operation } from 'fast-json-patch';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Course } from '../models/course.model';

@Injectable({
  providedIn: 'root'
})
export class CoursesService {

  constructor(private _http: HttpClient) { }

  API = 'api/courses';

  getCourses(): Observable<Course[]> {
    return this._http.get<Course[]>(`${this.API}`);
  }

  getStudent(courseId: number): Observable<Course> {
    return this._http.get<Course>(`${this.API}/${courseId}`);
  }

  addStudent(courseToAdd: Course): Observable<Course> {
    return this._http.post<Course>(`${this.API}`, courseToAdd,
      { headers: { 'Content-Type': 'application/json' } });
  }

  partiallyUpdateStudent(courseId: number, patchDocument: Operation[]): Observable<any> {
    return this._http.patch(`${this.API}/${courseId}`, patchDocument,
      { headers: { 'Content-Type': 'application/json' } });
  }
}
