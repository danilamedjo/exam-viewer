import { Operation } from 'fast-json-patch';
import { Exam } from './../models/exam.model';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ExamsService {

  constructor(private _http: HttpClient) { }

  API = 'api/exams';

  getExams(): Observable<Exam[]> {
    return this._http.get<Exam[]>(`${this.API}`);
  }

  getStudent(examId: number): Observable<Exam> {
    return this._http.get<Exam>(`${this.API}/${examId}`);
  }

  addStudent(examToAdd: Exam): Observable<Exam> {
    return this._http.post<Exam>(`${this.API}`, examToAdd,
      { headers: { 'Content-Type': 'application/json' } });
  }

  partiallyUpdateStudent(examId: number, patchDocument: Operation[]): Observable<any> {
    return this._http.patch(`${this.API}/${examId}`, patchDocument,
      { headers: { 'Content-Type': 'application/json' } });
  }
}
