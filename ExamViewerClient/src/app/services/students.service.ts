import { Student } from './../models/student.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Operation } from 'fast-json-patch';

@Injectable({
  providedIn: 'root'
})
export class StudentsService {

  constructor(private _http: HttpClient) { }

  API = 'api/students';

  getStudents(): Observable<Student[]> {
    return this._http.get<Student[]>(`${this.API}`);
  }

  getStudent(studentId: number): Observable<Student> {
    return this._http.get<Student>(`${this.API}/${studentId}`);
  }

  addStudent(studentToAdd: Student): Observable<Student> {
    return this._http.post<Student>(`${this.API}`, studentToAdd,
      { headers: { 'Content-Type': 'application/json' } });
  }

  partiallyUpdateStudent(studentId: number, patchDocument: Operation[]): Observable<any> {
    return this._http.patch(`${this.API}/${studentId}`, patchDocument,
      { headers: { 'Content-Type': 'application/json' } });
  }

}
