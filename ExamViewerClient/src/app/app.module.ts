import { StudentsTableComponent } from './components/students-table/students-table.component';
import { StudentsService } from './services/students.service';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatMenuModule } from '@angular/material/menu';
import { HomeComponent } from './components/home/home.component';
import { AppComponent } from './app.component';
import { MatInputModule } from '@angular/material/input';
import { DatePipe } from '@angular/common';

import { AppRoutingModule } from './app-routing-module';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HeaderComponent } from './components/header/header.component';
import { MatButtonModule, MatExpansionModule, MatTableModule, MatPaginatorModule, MatSortModule } from '@angular/material';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
    MatMenuModule,
    MatButtonModule,
    MatToolbarModule,
    MatExpansionModule,
    MatTableModule,
    MatFormFieldModule,
    MatInputModule,
    MatPaginatorModule,
    MatSortModule,
  ],
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    StudentsTableComponent
  ],
  providers: [StudentsService, DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule {
}
