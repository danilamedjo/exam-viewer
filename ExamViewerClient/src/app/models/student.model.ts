import { Exam } from './exam.model';

export interface Student {
    id: number;
    studentIndex: string;
    firstName: string;
    lastName: string;
    exams: Exam[];
}
