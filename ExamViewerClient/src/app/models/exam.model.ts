export interface Exam {
    subjectName: string;
    subjectProfessorFirstName: string;
    subjectProfessorLastName: string;
    date: Date;
    passed: boolean;
}
