import { StudentsTableComponent } from './components/students-table/students-table.component';

import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { Routes } from '@angular/router';



const routes: Routes = [
  { path: '', component: StudentsTableComponent, data: { title: 'home' } },
  { path: 'students-table', component: StudentsTableComponent, data: { title: 'Students List'} },
  { path: '**', component: StudentsTableComponent, data: { title: 'Home' } }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule { }
