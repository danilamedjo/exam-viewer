﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ExamViewerAPI.Dtos;
using ExamViewerAPI.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ExamViewerAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/students")]
    public class StudentsController : Controller
    {
        private readonly ILogger<StudentsController> _logger;
        private readonly IExamViewerRepository _examViewerRepository;

        public StudentsController(ILogger<StudentsController> logger, IExamViewerRepository examViewerRepository)
        {
            _logger = logger;
            _examViewerRepository = examViewerRepository;
        }

        [HttpGet]
        public async Task<IActionResult> GetStudents()
        {
            IEnumerable<Entities.Student> studentsFromRepo = new List<Entities.Student>();
            studentsFromRepo = await _examViewerRepository.GetStudents();
            var students = Mapper.Map<IEnumerable<StudentDto>>(studentsFromRepo);
            _logger.LogInformation("Students are retreived");
            return Ok(students);
        }

        [HttpGet("{studentId}")]
        public async Task<IActionResult> GetStudent(int studentId)
        {
            var studentFromRepo = await _examViewerRepository.GetStudent(studentId);
            _logger.LogInformation($"Student with id {studentId} is retreived");
            return Ok(Mapper.Map<StudentDto>(studentFromRepo));
        }

        [HttpPost]
        public async Task<IActionResult> AddStudent([FromBody] StudentDto student)
        {
            {
                if (student == null)
                {
                    return BadRequest();
                }

                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var studentEntity = Mapper.Map<Entities.Student>(student);

                await _examViewerRepository.AddStudent(studentEntity);

                if (!await _examViewerRepository.SaveAsync())
                {
                    throw new Exception("Adding a student failed on save.");
                }

                var studentToReturn = Mapper.Map<StudentDto>(studentEntity);

                return CreatedAtRoute("GetStudent", new { student = studentToReturn.Id }, studentToReturn);
            }
        }

        [HttpPatch("{studentId}")]
        public async Task<IActionResult> PartiallyUpdateStudent(int studentId,
          [FromBody] JsonPatchDocument<StudentDto> jsonPatchDocument)
        {
            if (jsonPatchDocument == null)
            {
                return BadRequest();
            }

            var studentFromRepo = await _examViewerRepository.GetStudent(studentId);

            if (studentFromRepo == null)
            {
                return BadRequest();
            }

            var studentToPatch = Mapper.Map<StudentDto>(studentFromRepo);

            jsonPatchDocument.ApplyTo(studentToPatch, ModelState);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Mapper.Map(studentToPatch, studentFromRepo);

            await _examViewerRepository.UpdateStudent(studentFromRepo);

            if (!await _examViewerRepository.SaveAsync())
            {
                throw new Exception("Updating a student failed on save.");
            }

            return NoContent();
        }

        [HttpDelete("studentId")]
        public async Task<IActionResult> DeleteStudent(int studentId)
        {
            var studentFromRepo = await _examViewerRepository.GetStudent(studentId);
            if (studentFromRepo == null)
            {
                return BadRequest();
            }
            _examViewerRepository.DeleteStudent(studentFromRepo);
            _logger.LogInformation($"Student with id {studentId} is deleted");
            return NoContent();
        }


    }
}
