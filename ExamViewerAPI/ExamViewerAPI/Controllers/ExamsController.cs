﻿using AutoMapper;
using ExamViewerAPI.Dtos;
using ExamViewerAPI.Services;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExamViewerAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/exams")]
    public class ExamsController : Controller
    {
        private ILogger<ExamsController> _logger;
        private readonly IExamViewerRepository _examViewerRepository;
        private readonly IExamValidation _examValidation;
        
        public ExamsController(ILogger<ExamsController> logger, IExamViewerRepository examViewerRepository, IExamValidation examValidation)
        {
            _logger = logger;
            _examViewerRepository = examViewerRepository;
            _examValidation = examValidation;
            
        }
        [HttpGet]
        public async Task<IActionResult> GetExams()
        {
            IEnumerable<Entities.Exam> examsFromRepo = new List<Entities.Exam>();
            examsFromRepo = await _examViewerRepository.GetExams();
            var exams = Mapper.Map<IEnumerable<ExamDto>>(examsFromRepo);
            _logger.LogInformation("Exams are retrieved");
            return Ok(exams);
        }

        [HttpGet("{examId}")]
        public async Task<IActionResult> GetExam(int examId)
        {
            var examFromRepo = await _examViewerRepository.GetExam(examId);
            _logger.LogInformation($"Exam with id {examId} is retreived");
            return Ok(Mapper.Map<ExamDto>(examFromRepo));
        }

        [HttpPost]
        public async Task<IActionResult> AddExam([FromBody] ExamDto exam)
        {
            {   

                if (exam == null)
                {
                    return BadRequest();
                }

                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                if(_examValidation.MaxNumberOfExamsValidation(exam.StudentId, exam.SubjectId, exam.Date))
                {
                    return Forbid();
                }

                var examEntity = Mapper.Map<Entities.Exam>(exam);

                await _examViewerRepository.AddExam(examEntity);

                if (!await _examViewerRepository.SaveAsync())
                {
                    throw new Exception("Adding a exam failed on save.");
                }

                var examToReturn = Mapper.Map<ExamDto>(examEntity);

                return CreatedAtRoute("GetExam", new { exam = examToReturn.Id }, examToReturn);
            }
        }

        [HttpPatch("{examId}")]
        public async Task<IActionResult> PartiallyUpdateStudent(int examId,
          [FromBody] JsonPatchDocument<ExamDto> jsonPatchDocument)
        {
            if (jsonPatchDocument == null)
            {
                return BadRequest();
            }

            var examFromRepo = await _examViewerRepository.GetExam(examId);

            if (examFromRepo == null)
            {
                return BadRequest();
            }

            var examToPatch = Mapper.Map<ExamDto>(examFromRepo);

            jsonPatchDocument.ApplyTo(examToPatch, ModelState);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Mapper.Map(examToPatch, examFromRepo);

            await _examViewerRepository.UpdateExam(examFromRepo);

            if (!await _examViewerRepository.SaveAsync())
            {
                throw new Exception("Updating a exam failed on save.");
            }

            return NoContent();
        }

        [HttpDelete("examId")]
        public async Task<IActionResult> DeleteExam(int examId)
        {
            var examFromRepo = await _examViewerRepository.GetExam(examId);
            if (examFromRepo == null)
            {
                return BadRequest();
            }
            _examViewerRepository.DeleteExam(examFromRepo);
            _logger.LogInformation($"Student with id {examId} is deleted");
            return NoContent();
        }



        //Validation required when creating or updating exams
    }
}