﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ExamViewerAPI.Dtos;
using ExamViewerAPI.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ExamViewerAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/courses")]
    public class CoursesController : Controller
    {
        private ILogger<ExamsController> _logger;
        private IExamViewerRepository _examViewerRepository;

        public CoursesController(ILogger<ExamsController> logger, IExamViewerRepository examViewerRepository)
        {
            _logger = logger;
            _examViewerRepository = examViewerRepository;
        }

        [HttpGet]
        public async Task<IActionResult> GetCourses()
        {
            IEnumerable<Entities.Course> coursesFromRepo = new List<Entities.Course>();
            coursesFromRepo = await _examViewerRepository.GetCourses();
            var courses = Mapper.Map<IEnumerable<CourseDto>>(coursesFromRepo);
            _logger.LogInformation("Courses are retreived");
            return Ok(courses);
        }

        [HttpGet("{courseId}")]
        public async Task<IActionResult> GetCourse(int courseId)
        {
            var courseFromRepo = await _examViewerRepository.GetCourse(courseId);
            _logger.LogInformation($"Course with id {courseId} is retreived");
            return Ok(Mapper.Map<CourseDto>(courseFromRepo));
        }

        [HttpPost]
        public async Task<IActionResult> AddCourse([FromBody] CourseDto course)
        {
            {
                if (course == null)
                {
                    return BadRequest();
                }

                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var courseEntity = Mapper.Map<Entities.Course>(course);

                await _examViewerRepository.AddCourse(courseEntity);

                if (!await _examViewerRepository.SaveAsync())
                {
                    throw new Exception("Adding a course failed on save.");
                }

                var courseToReturn = Mapper.Map<CourseDto>(courseEntity);

                return CreatedAtRoute("GetCourse", new { course = courseToReturn.Id }, courseToReturn);
            }
        }

        [HttpPatch("{courseId}")]
        public async Task<IActionResult> PartiallyUpdateCourse(int courseId,
          [FromBody] JsonPatchDocument<CourseDto> jsonPatchDocument)
        {
            if (jsonPatchDocument == null)
            {
                return BadRequest();
            }

            var courseFromRepo = await _examViewerRepository.GetCourse(courseId);

            if (courseFromRepo == null)
            {
                return BadRequest();
            }

            var courseToPatch = Mapper.Map<CourseDto>(courseFromRepo);

            jsonPatchDocument.ApplyTo(courseToPatch, ModelState);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
           
            Mapper.Map(courseToPatch, courseFromRepo);

            await _examViewerRepository.UpdateCourse(courseFromRepo);

            if (!await _examViewerRepository.SaveAsync())
            {
                throw new Exception("Updating a course failed on save.");
            }

            return NoContent();
        }

        [HttpDelete("courseId")]
        public async Task<IActionResult> DeleteCourse(int courseId)
        {
            var courseFromRepo = await _examViewerRepository.GetCourse(courseId);
            if (courseFromRepo == null)
            {
                return BadRequest();
            }
            _examViewerRepository.DeleteCourse(courseFromRepo);
            _logger.LogInformation($"Course with id {courseId} is deleted");
            return NoContent();
        }


    }
}