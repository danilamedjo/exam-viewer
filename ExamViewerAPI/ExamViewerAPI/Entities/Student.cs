﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ExamViewerAPI.Entities
{
    [Table("Student")]
    public class Student
    {   
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string FirstName { get; set;}

        [Required]
        [StringLength(50)]
        public string LastName { get; set;}

        [Required]
        [StringLength(20)]
        public string StudentIndex { get; set;}

        public ICollection<Exam> Exams { get; set; }
            = new List<Exam>();
        
    }
}
