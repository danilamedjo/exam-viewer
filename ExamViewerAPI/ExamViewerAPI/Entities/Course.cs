﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ExamViewerAPI.Entities
{   
    [Table("Course")]
    public class Course
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CourseId { get; set; }

        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        public int ProfessorId { get; set; }
        
        [ForeignKey("ProfessorId")]
        public Professor Professor { get; set; }
                
        public ICollection<Exam> Exams { get; set; }
            = new List<Exam>();

        public ICollection<Student> Students { get; set; }
            = new List<Student>();
        
    }
}
