﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ExamViewerAPI.Entities
{
    [Table("Exams")]
    public class Exam
    {   
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ExamId { get; set; }

        [Required]
        public DateTime Date { get; set; }

        public int CourseId { get; set; }

        [ForeignKey("CourseId")]
        public Course Course;
        
        public int StudentId { get; set;}

        [ForeignKey("StudentId")]
        public Student Student { get; set;}

        public bool Passed {get; set;}
    }
}
