﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ExamViewerAPI.Services;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Microsoft.AspNetCore.Mvc.Formatters;
using System.Linq;

namespace ExamViewerAPI
{
    public class Startup
    {
        public static IConfiguration Configuration { get; private set; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddMvc(setupAction =>
                {
                    setupAction.ReturnHttpNotAcceptable = true;

                    var jsonOutputFormatter = setupAction.OutputFormatters
                       .OfType<JsonOutputFormatter>().FirstOrDefault();
                })
                .AddJsonOptions(options =>
                {
                    options.SerializerSettings.DateParseHandling = DateParseHandling.DateTime;
                    options.SerializerSettings.ContractResolver =
                        new CamelCasePropertyNamesContractResolver();
                });

            // Configure CORS so the API allows requests from JavaScript.  
            // For demo purposes, all origins/headers/methods are allowed.  
            services.AddCors(options =>
            {
                options.AddPolicy("AllowSpecificOrigin",
                    builder => builder.WithOrigins("http://localhost:4200").AllowAnyHeader().AllowAnyMethod());
            });

            // register the DbContext on the container, getting the connection string from
            // appsettings - using this during development environment
            var connectionString = Configuration["connectionString:examViewerDBConnectionString"];
            services.AddDbContext<ExamViewerContext>(o => o.UseSqlServer(connectionString));

            // register the repository
            services.AddScoped<IExamViewerRepository, ExamViewerRepository>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler(appBuilder =>
                {
                    appBuilder.Run(async context =>
                    {
                        context.Response.StatusCode = 500;
                        await context.Response.WriteAsync("An unexpected fault happened. Try again later.");
                    });
                });
            }

            app.Run(async (context) =>
            {
                await context.Response.WriteAsync("Hello World!");
            });

            AutoMapper.Mapper.Initialize(am =>
            {
                am.CreateMap<Entities.Exam, Dtos.ExamDto>();
                am.CreateMap<Entities.Student, Dtos.StudentDto>();
                am.CreateMap<Entities.Course, Dtos.CourseDto>();
            });

            // Enable CORS
            app.UseCors("AllowSpecificOrigin");

            app.UseMvc();
        }
    }
}
