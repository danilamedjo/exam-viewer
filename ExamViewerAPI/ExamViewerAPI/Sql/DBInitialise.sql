DROP SCHEMA IF EXISTS exam-viewer-db;
CREATE SCHEMA exam-viewer-db DEFAULT CHARACTER SET utf8;
USE exam-viewer-db;


CREATE TABLE students(
	student_id INT AUTO_INCREMENT,
	index_number VARCHAR(20) NOT NULL,
	firstname VARCHAR(20) NOT NULL,
	lastname VARCHAR(20) NOT NULL,
	PRIMARY KEY(student_id));
	
CREATE TABLE professors(
	professor_id INT AUTO_INCREMENT,
	name VARCHAR(20) NOT NULL,
	lastname VARCHAR(20) NOT NULL,
	PRIMARY KEY(professor_id))
	
CREATE TABLE courses (
	course_id INT AUTO_INCREMENT,
	name VARCHAR(20) NOT NULL,
	PRIMARY KEY(course_id)
)

CREATE TABLE professors_courses (
	professor_id INT NOT NULL,
	course_id INT NOT NULL,
	PRIMARY KEY(professor_id, course_id),
	
	FOREIGN KEY (professor_id) REFERENCES professors(professor_id)
	    ON DELETE CASCADE, 
	FOREIGN KEY (course_id) REFERENCES courses(course_id)
	    ON DELETE CASCADE
)
	
CREATE TABLE students_courses (
	student_id INT NOT NULL,
	course_id INT NOT NULL,
	PRIMARY KEY(student_id, course_id),
	
	FOREIGN KEY (student_id) REFERENCES students(student_id)
		ON DELETE CASCADE,
	FOREIGN KEY (course_id) REFERENCES courses(course_id)
		ON DELETE CASCADE
)
	
CREATE TABLE exams (
	student_id INT NOT NULL,
	course_id INT NOT NULL,
	examDate DATE NOT NULL,
	passed BIT,
	PRIMARY KEY (student_id, course_id),
	
	FOREIGN KEY (student_id) REFERENCES students(student_id)
		ON DELETE CASCADE,
	FOREIGN KEY (course_id) REFERENCES courses(course_id)
		ON DELETE CASCADE
)