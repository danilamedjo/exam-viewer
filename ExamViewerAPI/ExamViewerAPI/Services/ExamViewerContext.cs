﻿using ExamViewerAPI.Entities;
using Microsoft.EntityFrameworkCore;

namespace ExamViewerAPI.Services
{
    public class ExamViewerContext : DbContext
    {
        public ExamViewerContext(DbContextOptions<ExamViewerContext> options )
          : base(options)
        { 

        }

        public DbSet<Student> Students { get; set; }
        public DbSet<Exam> Exams { get; set; }
        public DbSet<Professor> Professors { get; set; }
        public DbSet<Course> Courses { get; set; }
    }
}
