﻿using System;

namespace ExamViewerAPI.Services
{
    public interface IExamValidation
    {
        bool MaxNumberOfExamsValidation(int studentId, int courseId, DateTime date);

    }
}