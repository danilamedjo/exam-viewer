﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ExamViewerAPI.Entities;
using Microsoft.EntityFrameworkCore;

namespace ExamViewerAPI.Services
{
    public class ExamViewerRepository : IExamViewerRepository, IExamValidation
    {
        private ExamViewerContext _context;

        public ExamViewerRepository(ExamViewerContext context)
        {
            _context = context;
        }

        public async Task AddCourse(Course course)
        {
            await _context.Courses.AddAsync(course);
        }

        public async Task AddExam(Exam exam)
        {
            await _context.Exams.AddAsync(exam);
        }

        public async Task AddStudent(Student student)
        {
            await _context.Students.AddAsync(student);
        }

        public void DeleteCourse(Course course)
        {
            _context.Remove(course);
        }

        public void DeleteExam(Exam exam)
        {
            _context.Remove(exam);
        }

        public void DeleteStudent(Student student)
        {
            _context.Remove(student);
        }

        public async Task<Course> GetCourse(int courseId)
        {
            return await _context.Courses.Include(c => c.CourseId == courseId).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<Course>> GetCourses()
        {
            return await _context.Courses.OrderBy(c => c.Name).ToListAsync();
        }

        public async Task<Exam> GetExam(int examId)
        {
            return await _context.Exams.Include(e => e.ExamId == examId).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<Exam>> GetExams()
        {
            return await _context.Exams.OrderBy(e => e.Date).ToListAsync();
        }

        public async Task<Student> GetStudent(int studentId)
        {
            return await _context.Students.Include(s => s.Id == studentId).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<Student>> GetStudents()
        {
            return await _context.Students.OrderBy(s => s.FirstName).ToListAsync();
        }

        public async Task<bool> SaveAsync()
        {
            return (await _context.SaveChangesAsync() >= 0);
        }

        public Task UpdateCourse(Course course)
        {
            throw new NotImplementedException();
        }

        public Task UpdateExam(Exam exam)
        {
            throw new NotImplementedException();
        }

        public Task UpdateStudent(Student student)
        {
            throw new NotImplementedException();
        }
        
        public bool MaxNumberOfExamsValidation(int studentId, int courseId, DateTime date)
        {
            var tempExSt = _context.Exams.Where(e => e.StudentId == studentId && e.CourseId == courseId && e.Date == date).ToList();
            if (tempExSt.Count >= 5)
            {
                return false;
            }
            return true;
        }

        public async Task<IEnumerable<Exam>> GetAllPassedExamsInDateRange(int professorId, DateTime from, DateTime to)
        {
            // SQL Query in ~/ExamViewerAPI/Sql/StudentsWithExamPassList.sql

            return await _context.Exams.Where(e => e.Passed == true && e.Date.CompareTo(from) > 0 && e.Date.CompareTo(to) < 0).ToListAsync();
        }
    }
}
