﻿using ExamViewerAPI.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ExamViewerAPI.Services
{
    public interface IExamViewerRepository
    {
        Task<IEnumerable<Student>> GetStudents();
        Task<Student> GetStudent(int studentId);
        Task AddStudent(Student student);
        Task UpdateStudent(Student student);
        void DeleteStudent(Student student);
        

        Task<IEnumerable<Exam>> GetExams();
        Task<Exam> GetExam(int examId);
        Task AddExam(Exam exam);
        Task UpdateExam(Exam exam);
        void DeleteExam(Exam exam);

        Task<IEnumerable<Course>> GetCourses();
        Task<Course> GetCourse(int courseId);
        Task AddCourse(Course course);
        Task UpdateCourse(Course course);
        void DeleteCourse(Course course);

        Task<IEnumerable<Exam>> GetAllPassedExamsInDateRange(int professorId, DateTime from, DateTime to);

        Task<bool> SaveAsync();

    }
}
 